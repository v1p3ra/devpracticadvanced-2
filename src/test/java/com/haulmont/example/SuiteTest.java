package com.haulmont.example;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@Suite.SuiteClasses({NumberServiceTest.class, StringServiceTest.class})
@RunWith(Suite.class)
public class SuiteTest {
}
