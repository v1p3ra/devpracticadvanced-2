package com.haulmont.example.categories;

import com.haulmont.example.NumberServiceTest;
import com.haulmont.example.StringServiceTest;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Categories.class)
@Categories.IncludeCategory({StringTestCategory.class, NumberTestCategory.class})
@Suite.SuiteClasses({NumberServiceTest.class, StringServiceTest.class})
public class CategoryTest {
}
