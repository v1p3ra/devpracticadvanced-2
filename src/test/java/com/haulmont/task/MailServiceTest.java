package com.haulmont.task;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class MailServiceTest {
    @Mock
    private MailSender mailSenderMock;
    @Mock
    private MessageRepository messageRepositoryMock;
    @Spy
    private MessageRepository messageRepositorySpy;

    private MailService mailService;
    private Message message;

    @Before
    public void setUp() {
        Person person1 = new Person();
        person1.setEmail("");
        Person person2 = new Person();
        person2.setEmail("");
        message = new Message("", person1, person2, "");
    }

    public void setUpMock() {
        mailService = new MailService(mailSenderMock, messageRepositoryMock);

        Mockito.doAnswer(invocation -> {
            Message message = invocation.getArgument(0);
            message.setStatus(Status.REMOVED);
            return null;
        }).when(messageRepositoryMock).removeMessage(any());
    }

    public void setUpSpy() {
        mailService = new MailService(mailSenderMock, messageRepositorySpy);
        Mockito.doNothing().when(messageRepositorySpy).removeMessage(message);
    }

    @Test
    public void testSendMessageWithSuccess() {
        setUpMock();
        Mockito.when(mailSenderMock.sendMessage(anyString(), anyString(), anyString(), anyString())).thenReturn(true);
        boolean result = mailService.sendMessage(message);
        assertTrue(result);
        assertEquals(Status.DELIVERED, message.getStatus());
    }

    @Test
    public void testSendMessageWithError() {
        setUpMock();
        Mockito.when(mailSenderMock.sendMessage(anyString(), anyString(), anyString(), anyString())).thenReturn(false);
        boolean result = mailService.sendMessage(message);
        assertFalse(result);
        assertEquals(Status.ERROR, message.getStatus());
    }

    @Test
    public void isSendMessageMethodCalled() {
        setUpMock();
        Mockito.when(mailSenderMock.sendMessage(anyString(), anyString(), anyString(), anyString())).thenReturn(true);
        mailService.sendMessage(message);
        Mockito.verify(mailSenderMock, Mockito.times(1))
                .sendMessage(anyString(), anyString(), anyString(), anyString());
    }

    @Test
    public void isSendMessageMethodCalledWithRightParameters() {
        setUpMock();
        Mockito.when(mailSenderMock.sendMessage(anyString(), anyString(), anyString(), anyString())).thenReturn(true);
        mailService.sendMessage(message);
        Mockito.verify(mailSenderMock, Mockito.times(1))
                .sendMessage(message.getFrom().getEmail(), message.getTo().getEmail(), message.getSubject(), message.getText());
    }

    @Test
    public void testRemoveMessageMock() {
        setUpMock();
        mailService.removeMessage(message);
        assertEquals(message.getStatus(), Status.REMOVED);
    }

    @Test
    public void testRemoveMessageSpy() {
        setUpSpy();
        mailService.removeMessage(message);
        assertNotEquals(message.getStatus(), Status.REMOVED);
    }
}
